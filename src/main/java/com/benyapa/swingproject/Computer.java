/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.benyapa.swingproject;

import java.util.concurrent.ThreadLocalRandom;

/**
 *
 * @author bwstx
 */
public class Computer {

    private int hand; //คอม
    private int playerHand; //rock0,scis1,paper2
    private int win, lose, draw;
    private int status;

    public Computer() {

    }

    private int choob() {
        return ThreadLocalRandom.current().nextInt(0, 3);
    }

    public int paoYingChoob(int playerHand) { //win1,draw0,lose-1
        this.playerHand = playerHand;
        this.hand = choob(); //สั่งให้คอมแรนด้อมออกมาเอง
        if (this.playerHand == this.hand) {//draw
            draw++;
            status = 0;
            return 0;
        }
        // win
        if (this.playerHand == 0 && this.hand == 1) {
            win++;
            status = 1;
            return 1;
        }
        if (this.playerHand == 1 && this.hand == 2) {
            win++;
            status = 1;
            return 1;
        }
        if (this.playerHand == 2 && this.hand == 0) {
            win++;
            status = 1;
            return 1;
        }
        lose++;
        status = -1;
        return -1; // lose
    }

    public int getHand() {
        return hand;
    }

    public int getPlayerHand() {
        return playerHand;
    }

    public int getWin() {
        return win;
    }

    public int getLose() {
        return lose;
    }

    public int getDraw() {
        return draw;
    }

    public int getStatus() {
        return status;
    }

}
